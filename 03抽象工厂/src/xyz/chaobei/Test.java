package xyz.chaobei;

import xyz.chaobei.factory.AbstractFactory;
import xyz.chaobei.impl.SpringStyleFactory;
import xyz.chaobei.product.Button;
import xyz.chaobei.product.TextField;
import xyz.chaobei.product.Window;
import xyz.chaobei.util.XmlUtils;

public class Test {

    public static void main(String[] args) {

        AbstractFactory factory = new SpringStyleFactory();
        Button button = factory.createButton();
        Window window = factory.createWindow();
        TextField textField = factory.createTextField();

        button.display();
        window.display();
        textField.display();

        /**
         * xml的方式，满足开闭原则
         */
        AbstractFactory colorFactory = (AbstractFactory) XmlUtils.getBean("color-factory");
        Button button1 = colorFactory.createButton();
        Window window1 = colorFactory.createWindow();
        TextField textField1 = colorFactory.createTextField();

        button1.display();
        window1.display();
        textField1.display();
    }

}
