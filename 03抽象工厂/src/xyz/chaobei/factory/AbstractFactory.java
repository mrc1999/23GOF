package xyz.chaobei.factory;

import xyz.chaobei.product.Button;
import xyz.chaobei.product.TextField;
import xyz.chaobei.product.Window;

public abstract class AbstractFactory {

    public abstract Button createButton();

    public abstract Window createWindow();

    public abstract TextField createTextField();
}
