package xyz.chaobei.util;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XmlUtils {

    public static Object getBean(String name) {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new File("config.xml"));
            NodeList root = document.getElementsByTagName(name);

            String classes = root.item(0).getFirstChild().getNodeValue();
            return Class.forName(classes).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}