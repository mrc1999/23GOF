package xyz.chaobei.impl;

import xyz.chaobei.product.Button;

public class RedButton implements Button {

    @Override
    public void display() {
        System.out.println("红色的按钮");
    }
}
