package xyz.chaobei.impl;

import xyz.chaobei.factory.AbstractFactory;
import xyz.chaobei.product.Button;
import xyz.chaobei.product.TextField;
import xyz.chaobei.product.Window;

public class SpringStyleFactory extends AbstractFactory {

    @Override
    public Button createButton() {
        return new GreenButton();
    }

    @Override
    public Window createWindow() {
        return new GreenWindow();
    }

    @Override
    public TextField createTextField() {
        return new GreenTextField();
    }
}
