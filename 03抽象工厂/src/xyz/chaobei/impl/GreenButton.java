package xyz.chaobei.impl;

import xyz.chaobei.product.Button;

public class GreenButton implements Button {

    @Override
    public void display() {
        System.out.println("绿色的按钮");
    }
}
