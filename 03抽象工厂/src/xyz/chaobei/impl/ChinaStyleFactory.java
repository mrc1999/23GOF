package xyz.chaobei.impl;

import xyz.chaobei.factory.AbstractFactory;
import xyz.chaobei.product.Button;
import xyz.chaobei.product.TextField;
import xyz.chaobei.product.Window;

public class ChinaStyleFactory extends AbstractFactory {

    @Override
    public Button createButton() {
        return new RedButton();
    }

    @Override
    public Window createWindow() {
        return new RedWindow();
    }

    @Override
    public TextField createTextField() {
        return new RedTextField();
    }
}
