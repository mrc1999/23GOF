package xyz.chaobei.example;

public class AndroidSystem extends SystemAbstractFactory {

    @Override
    public OperationController operationController() {
        System.out.println("开始适配操作控制器...");
        return new ScreenOperation();
    }

    @Override
    public InterfaceController interfaceController() {
        System.out.println("开始配置界面控制器所需的参数...");
        return new MobileInterface();
    }
}
