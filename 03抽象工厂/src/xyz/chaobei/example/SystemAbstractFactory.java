package xyz.chaobei.example;

/**
 * 抽象系统工厂，用于创建Symbian/Android/Windows Mobile
 */
public abstract class SystemAbstractFactory {

    /**
     * 操作控制
     * @return
     */
    abstract OperationController operationController();

    /**
     * 界面控制
     * @return
     */
    abstract InterfaceController interfaceController();

}
