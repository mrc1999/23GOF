package xyz.chaobei.example;

public class WindowsMobileSystem extends SystemAbstractFactory {

    @Override
    public OperationController operationController() {
        System.out.println("开始组装控制器参数...");
        return new KeyboardOperation();
    }

    @Override
    public InterfaceController interfaceController() {
        System.out.println("开始组装界面控制器");
        return new PcInterface();
    }
}
