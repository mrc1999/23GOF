package xyz.chaobei.example;

public class Main {

    public static void main(String[] args) {

        SystemAbstractFactory factory = new WindowsMobileSystem();
        InterfaceController interfaceController = factory.interfaceController();
        OperationController operationController = factory.operationController();

        interfaceController.controller();
        operationController.operation();

    }

}
