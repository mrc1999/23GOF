package xyz.chaobei.example;

/**
 * 游戏操作控制
 */
public interface OperationController {

    void operation();

}
