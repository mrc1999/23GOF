package xyz.chaobei.impl;

import xyz.chaobei.model.Shape;

/**
 * 圆形
 */
public class RoundShape implements Shape {

    @Override
    public void draw() {
        System.out.println("画出圆形");
    }

    @Override
    public void erase() {
        System.out.println("擦除圆形");
    }
}
