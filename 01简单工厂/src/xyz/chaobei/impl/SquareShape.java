package xyz.chaobei.impl;

import xyz.chaobei.model.Shape;

public class SquareShape implements Shape {
    @Override
    public void draw() {
        System.out.println("画出正方形");
    }

    @Override
    public void erase() {
        System.out.println("擦除正方形");
    }
}
