package xyz.chaobei.util;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

public class XmlUtils {

    public static String getShapeClass(String name) {

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new File("config.xml"));
            NodeList root = document.getElementsByTagName("shape-item");

            for (int i = 0; i < root.getLength(); i++) {
                Node node = root.item(i);
                String itemName = node.getAttributes().item(0).getNodeValue();

                if (name.equals(itemName)) {
                    return node.getFirstChild().getNodeValue();
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
