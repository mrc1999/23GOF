package xyz.chaobei;

import xyz.chaobei.factory.ShapeFactory;
import xyz.chaobei.factory.SimpleShapeFactory;
import xyz.chaobei.model.Shape;

public class Application {

    public static void main(String[] args) {
        Shape shape = SimpleShapeFactory.create("triangle");
        shape.draw();
        shape.erase();

        Shape round = ShapeFactory.create("round");
        round.draw();
        round.erase();
    }

}
