package xyz.chaobei.factory;

import xyz.chaobei.model.Shape;
import xyz.chaobei.util.XmlUtils;

/**
 * 满足开闭原则的简单工厂
 */
public class ShapeFactory {
    public static Shape create(String name) {
        String className = XmlUtils.getShapeClass(name);
        if (null == className) {
            throw new RuntimeException("未找到指定的配置");
        }
        try {
            Class classes = Class.forName(className);
            return (Shape) classes.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
