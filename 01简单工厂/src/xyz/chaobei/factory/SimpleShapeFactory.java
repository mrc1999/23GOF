package xyz.chaobei.factory;

import xyz.chaobei.impl.RoundShape;
import xyz.chaobei.impl.SquareShape;
import xyz.chaobei.impl.TriangleShape;
import xyz.chaobei.model.Shape;

/**
 * 简单的图形工厂
 */
public class SimpleShapeFactory {

    /**
     * <p>
     * <p>author: <a href='mailto:maruichao52@gmail.com'>MRC</a>
     *
     * @param name 名称
     * @return xyz.chaobei.model.Shape
     * @since 2021/7/29
     **/
    public static Shape create(String name) {
        if ("triangle".equals(name)) {
            return new TriangleShape();
        } else if ("round".equals(name)) {
            return new RoundShape();
        }
        /**
         * 违反开闭原则
         else if ("square".equals(name)) {
            return new SquareShape();
         }
         */
        throw new RuntimeException("未找到指定类型");
    }

}
