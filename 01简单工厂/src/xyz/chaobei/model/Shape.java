package xyz.chaobei.model;

/**
 * 图形接口
 */
public interface Shape {
    /**
     * 画图
     */
    void draw();
    /**
     * 擦除
     */
    void erase();
}
