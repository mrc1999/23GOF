package xyz.chaobei.impl;

import xyz.chaobei.model.Logger;
import xyz.chaobei.model.LoggerFactory;

public class DatabaseLoggerFactory extends LoggerFactory {

    @Override
    public Logger createLogger() {

        System.out.println("DatabaseLoggerFactory-初始化数据库连接");
        Logger logger = new DatabaseLogger();
        System.out.println("DatabaseLoggerFactory-做一些其他事情");

        return logger;
    }
}
