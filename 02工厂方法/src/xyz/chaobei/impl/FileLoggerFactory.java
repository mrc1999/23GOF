package xyz.chaobei.impl;

import xyz.chaobei.model.Logger;
import xyz.chaobei.model.LoggerFactory;

public class FileLoggerFactory extends LoggerFactory {
    @Override
    public Logger createLogger() {

        System.out.println("FileLoggerFactory-创建文件");
        Logger logger = new FileLogger();
        System.out.println("FileLoggerFactory-做一些其他工作");

        return logger;
    }
}
