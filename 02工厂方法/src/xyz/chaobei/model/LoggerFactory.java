package xyz.chaobei.model;

//public interface LoggerFactory {
//
//    Logger createLogger();
//}
public abstract class LoggerFactory {

    public void writeLog() {
        Logger logger = createLogger();
        logger.writeLog();
    }

    public abstract Logger createLogger();
}