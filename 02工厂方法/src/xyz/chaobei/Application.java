package xyz.chaobei;

import xyz.chaobei.impl.DatabaseLoggerFactory;
import xyz.chaobei.model.Logger;
import xyz.chaobei.model.LoggerFactory;
import xyz.chaobei.util.XmlUtils;

public class Application {

    public static void main(String[] args) {

        // 方式1
        LoggerFactory loggerFactory = new DatabaseLoggerFactory();
        Logger logger = loggerFactory.createLogger();
        logger.writeLog();

        // 方式2
        LoggerFactory configFactory = (LoggerFactory) XmlUtils.getBean("log-factory");
        Logger configLogger = configFactory.createLogger();
        configLogger.writeLog();

        // 进行隐藏Logger,直接调用工厂提供的方法。
        configLogger.writeLog();
    }
}
