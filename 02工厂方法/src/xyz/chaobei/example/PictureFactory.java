package xyz.chaobei.example;

import java.io.File;
import java.util.List;

public abstract class PictureFactory {

    abstract PictureReader create();

    /**
     * 提供访问点
     * @param dir
     * @return
     */
    public List<File> allFile(String dir) {
        return create().read(dir);
    }

}
