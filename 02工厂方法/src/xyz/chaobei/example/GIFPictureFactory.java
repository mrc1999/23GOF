package xyz.chaobei.example;

public class GIFPictureFactory extends PictureFactory {

    @Override
    PictureReader create() {
        System.out.println("读取文件前的准备...");
        return new GIFPictureReader();
    }
}
