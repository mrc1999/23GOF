package xyz.chaobei.example;

import java.io.File;
import java.util.List;

public class Main {


    public static void main(String[] args) {

        PictureFactory factory = new JPGPictureFactory();
        List<File> allGIF = factory.allFile(".\\picture");

        for (File file : allGIF) {
            System.out.println(file.getAbsolutePath());
        }

    }


}
