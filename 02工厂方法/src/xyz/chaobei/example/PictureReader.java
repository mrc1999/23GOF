package xyz.chaobei.example;

import java.io.File;
import java.util.List;

/**
 * 图片读取器
 */
public interface PictureReader {
    /**
     * 从指定目录下读取文件
     * @param basePath
     * @return
     */
    List<File> read(String basePath);
}
