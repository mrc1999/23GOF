package xyz.chaobei.example;

public class JPGPictureFactory extends PictureFactory {

    @Override
    PictureReader create() {
        System.out.println("创建读取器准备...");
        return new JPGPictureReader();
    }
}
