package xyz.chaobei.example;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class GIFPictureReader implements PictureReader {

    @Override
    public List<File> read(String basePath) {

        File file = new File(basePath);

        if (!file.isDirectory()) {
            throw new RuntimeException("非文件夹路径...");
        }
        File[] files = file.listFiles((desc) -> desc.getName().toLowerCase().endsWith(".gif"));

        return Arrays.asList(files);
    }

}
